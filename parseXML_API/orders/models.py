from django.db import models

# Create your models here.
class Order(models.Model):

	order_id = models.CharField(max_length=255, primary_key=True)

	marketplace = models.CharField(max_length=255)

	order_purchase_date = models.DateField(editable=True, 
		blank=True,null=True)
	
	order_amount = models.DecimalField(max_digits=5, 
		decimal_places=2)
	