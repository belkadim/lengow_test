from django.urls import path

from rest_framework import routers
from .views import OrderViewSet, parseXMLViews

router = routers.DefaultRouter()
router.register('', OrderViewSet)
