# Generated by Django 3.2.6 on 2021-08-13 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_alter_order_order_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_id',
            field=models.CharField(max_length=255, primary_key=True, serialize=False),
        ),
    ]
