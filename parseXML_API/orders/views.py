from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view, action
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from rest_framework import generics, status

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

import datetime
from .models import Order
from .parseXML import get_Orders
from .serializers import OrderSerializer, XMLSerializer



class OrderViewSet(GenericViewSet):

	queryset = Order.objects.all()
	serializer_class = OrderSerializer

	filter_backends = [DjangoFilterBackend, filters.SearchFilter, ]
	filterset_fields = ('order_id', 'marketplace', 'order_purchase_date', 'order_amount')
	search_fields = ('order_id', 'marketplace', 'order_purchase_date', 'order_amount')

	def filter_queryset(self, queryset):
		for backend in list(self.filter_backends):
			queryset = backend().filter_queryset(self.request, queryset, self)
		return queryset


	def list(self, request):
		queryset = self.filter_queryset(self.queryset)
		serializer = OrderSerializer(queryset, many=True)
		return Response(serializer.data)


	def create(self, request, *args, **kwargs):
		order_data = request.data

		if Order.objects.filter(order_id=order_data['order_id']).count()==0 :
			new_order = Order.objects.create(order_id=order_data['order_id'],
				marketplace=order_data['marketplace'], order_purchase_date=
				order_data['order_purchase_date'] if order_data['order_purchase_date']
				else datetime.date.today().strftime('%Y-%m-%d'), order_amount=order_data
				['order_amount'])
			new_order.save()
			serializer = OrderSerializer(new_order)

			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


	def retrieve(self, request, pk=None):

		order_object = get_object_or_404(self.queryset, pk=pk)
		serializer = OrderSerializer(order_object)
		return Response(serializer.data)

	def put(self, request, *args, **kwargs):

		order_object = self.get_order(request.data['order_id'])

		order_object.order_id = request.data['order_id']
		order_object.marketplace = request.data['marketplace']
		order_object.order_purchase_date = request.data['order_purchase_date']
		order_object.order_amount = request.data['order_amount']

		order_object.save()

		serializer = OrderSerializer(order_object)
		
		return Response(serializer.data)


	def get_order(self, pk):
		try:
			return Order.objects.get(pk=pk)
		except Order.DoesNotExist:
			raise Http404



class parseXMLViews(generics.GenericAPIView):
	
	serializer_class = XMLSerializer
	queryset = get_Orders()

	def get(self, request):
		
		serializer = XMLSerializer({
			'data': self.queryset, 
			})
		return Response(serializer.data)

	def put(self, request):
		objs = [Order(
			    order_id=ele['order_id'],
			    marketplace=ele['marketplace'],
			    order_purchase_date=ele['order_purchase_date'],
			    order_amount=ele['order_amount']
			) for ele in self.queryset if Order.objects.filter(order_id=ele['order_id']).count()==0]
		new_order = Order.objects.bulk_create(objs)

		serializer = OrderSerializer(self.queryset, many=True)

		return Response(serializer.data)