from rest_framework import serializers
from .models import Order

class OrderSerializer(serializers.ModelSerializer):

	class Meta:
		model = Order
		fields = ('__all__')

	lookup_field = 'order_id'
		
class XMLSerializer(serializers.Serializer):
	data = serializers.ListField(child=serializers.DictField())