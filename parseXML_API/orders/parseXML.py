import requests
from xml.dom.minidom import parse, parseString, Element


def get_Orders():
	url = 'http://test.lengow.io/orders-test.xml'
	response = requests.get(url)
	content = response.text
	xmlparse = parseString(content)
	Received = ['order_id', 'marketplace', 'order_purchase_date', 'order_amount']

	Data = []

	orders = xmlparse.getElementsByTagName('order')
	
	for i in range(len(orders)):
		Data.append({})
		for attr in orders[i].childNodes:
			inc = 0
			if attr.tagName in Received:
				if attr.childNodes:
					Data[i][attr.tagName] = attr.childNodes[0].data
				else:
					Data[i][attr.tagName] = None
	return Data

