"""parseXML_API URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from orders.urls import router as order_router

from orders.views import parseXMLViews

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title='Orders API - Lengow-test',
        default_version='v1',
        description="- Tester les commandes recuperer, ajouter, modifier," \
        " rechercher et filter pour la ViewSet Orders \n" \
        "- Stocker des objets du modele Order à partir d'un fichier XML analysé." \
        " (modele d'utilisation = GenericAPIview)",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@lengow.local"),
        license=openapi.License(name='Test License'),
    ),
    public=True,
)


router = routers.DefaultRouter()
router.registry.extend(order_router.registry)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('orders/', include(router.urls)),
    path('parsexml/', parseXMLViews.as_view()),
    path('', schema_view.with_ui('swagger', cache_timeout=0)
        , name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0)
        , name='schema-redoc'),
]
